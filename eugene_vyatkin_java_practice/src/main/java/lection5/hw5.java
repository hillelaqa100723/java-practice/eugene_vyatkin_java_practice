package lection5;

public class hw5 {
    public static void main(String[] args) {
        int[][] array = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };

        System.out.println();
        printArray(array);

        int[] rowSums = calculateRowSums(array);

        System.out.print("Result: ");
        for (int sum : rowSums) {
            System.out.print(sum + " ");
        }
        System.out.println();
    }

    public static int[] calculateRowSums(int[][] array) {
        int rows = array.length;
        int columns = array[0].length;
        int[] rowSums = new int[rows];

        for (int i = 0; i < rows; i++) {
            int sum = 0;
            for (int j = 0; j < columns; j++) {
                sum += array[i][j];
            }
            rowSums[i] = sum;
        }

        return rowSums;
    }

    public static void printArray(int[][] array) {
        for (int[] row : array) {
            for (int num : row) {
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}
