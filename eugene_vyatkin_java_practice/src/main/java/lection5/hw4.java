package lection5;

public class hw4 {
    public static void main(String[] args){
        int[][] array =
                {
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                };
        int rows = array.length;
        int columns = array[0].length;

        System.out.print("Main diagonal: ");
        for (int i = 0; i < rows; i++) {
            System.out.print(array[i][i] + " ");
        }
        System.out.println();

        System.out.print("Secondary diagonal: ");
        for (int i = 0; i < rows; i++) {
            System.out.print(array[i][columns - i - 1] + " ");
            }
            System.out.println();
        }
    }
