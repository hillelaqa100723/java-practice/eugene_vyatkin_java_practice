package hw1;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter first number: ");
        int a = in.nextInt();
        System.out.println("Enter second number: ");
        int b = in.nextInt();
        int rez;

        rez = a + b;
        System.out.println(rez);
        rez = a - b;
        System.out.println(rez);
        rez = a * b;
        System.out.println(rez);
        if (b != 0) {
            rez = a / b;
            System.out.println(rez);
            rez = a % b;
            System.out.println(rez);
        } else {
            System.out.println("You can't divide by 0");
        }
    }
}

