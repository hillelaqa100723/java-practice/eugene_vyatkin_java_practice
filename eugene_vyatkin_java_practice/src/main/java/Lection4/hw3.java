package Lection4;

public class hw3 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 4, 3, 2, 1};

        System.out.println("Array from the beginning:");
        for (int num : array) {
            System.out.print(num + " ");
        }

        System.out.println("\nArray from the end:");
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }

        boolean isPalindrome = true;
        for (int i = 0; i < array.length / 2; i++) {
            if (array[i] != array[array.length - 1 - i]) {
                isPalindrome = false;
                break;
            }
        }

        if (isPalindrome) {
            System.out.println("\nThe array is a palindrome.");
        } else {
            System.out.println("\nThe array is not a palindrome.");
        }
    }
}
